import './bootstrap';
import Vue from 'vue';
import App from './App';
import router from './router';
import mixin from './mixins';
import store from './store';
import Api from './api';
import Filters from './filters';
import Components from './components';
import Directive from './directive';

Vue.mixin(mixin);
Vue.use(Api);
Vue.use(Filters);
Vue.use(Components);
Vue.use(Directive);

Vue.config.productionTip = (process.env.NODE_ENV === 'development');

window.app = new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
});
